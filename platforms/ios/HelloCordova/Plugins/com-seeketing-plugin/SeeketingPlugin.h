#import <Cordova/CDV.h>

@interface SeeketingPlugin : CDVPlugin

- (void)trackEventNavi:(CDVInvokedUrlCommand*)command;
- (void)trackEventText:(CDVInvokedUrlCommand*)command;
- (void)trackEventClick:(CDVInvokedUrlCommand*)command;

@end
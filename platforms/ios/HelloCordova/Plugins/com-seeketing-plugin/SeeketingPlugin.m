#import "SeeketingPlugin.h"
#import <Cordova/CDV.h>
#import "SETS.h"

@implementation SeeketingPlugin

- (void)trackEventNavi:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        NSString* name = [command.arguments objectAtIndex:0];
        
        if (name != nil && [name length] > 0) {
            [SETS trackEventNavi:[command.arguments objectAtIndex:0] withPercent:[[command.arguments objectAtIndex:1] intValue] andInfo:[command.arguments objectAtIndex:2]];
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:name];
        } else {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        
        // The sendPluginResult method is thread-safe.
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)trackEventText:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        NSString* name = [command.arguments objectAtIndex:0];
        
        if (name != nil && [name length] > 0) {
            [SETS trackEventText:[command.arguments objectAtIndex:0] withParam:[command.arguments objectAtIndex:1] andInfo:[command.arguments objectAtIndex:2]];
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:name];
        } else {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        
        // The sendPluginResult method is thread-safe.
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)trackEventClick:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        NSString* name = [command.arguments objectAtIndex:0];
        
        if (name != nil && [name length] > 0) {
            [SETS trackEventClick:[command.arguments objectAtIndex:0]];
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:name];
        } else {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        
        // The sendPluginResult method is thread-safe.
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

@end
cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/com-seeketing-plugin/www/SeeketingPlugin.js",
        "id": "com-seeketing-plugin.SeeketingPlugin",
        "pluginId": "com-seeketing-plugin",
        "clobbers": [
            "seeketingplugin"
        ]
    },
    {
        "file": "plugins/cordova-plugin-console/www/console-via-logger.js",
        "id": "cordova-plugin-console.console",
        "pluginId": "cordova-plugin-console",
        "clobbers": [
            "console"
        ]
    },
    {
        "file": "plugins/cordova-plugin-console/www/logger.js",
        "id": "cordova-plugin-console.logger",
        "pluginId": "cordova-plugin-console",
        "clobbers": [
            "cordova.logger"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "com-seeketing-plugin": "1.0.0",
    "cordova-plugin-whitelist": "1.2.2",
    "cordova-plugin-console": "1.0.3"
}
// BOTTOM OF METADATA
});
cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/com-seeketing-plugin/www/SeeketingPlugin.js",
        "id": "com-seeketing-plugin.SeeketingPlugin",
        "clobbers": [
            "seeketingplugin"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-whitelist": "1.2.2",
    "com-seeketing-plugin": "1.0.0",
    "cordova-plugin-console": "1.0.3"
};
// BOTTOM OF METADATA
});
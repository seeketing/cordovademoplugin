/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package io.cordova.hellocordova;

import android.os.Bundle;

import com.seeketing.sdks.refs.Refs;
import com.seeketing.sdks.sets.Sets;
import com.seeketing.sdks.sets.UtilComms;

import org.apache.cordova.*;

public class MainActivity extends CordovaActivity
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Refs.startSession(this, true, true, 20152015121L, "691122524766", UtilComms.REM_PUB_13_TEST);
        Refs.lookForNotifications(this, getIntent().getExtras(), null);
        // Set by <content src="index.html" /> in config.xml
        loadUrl(launchUrl);
    }

    /**
     @name onResume
     @desc We will resume the SDK session state when the app return from background to foreground
     */
    @Override
    protected void onResume() {
        super.onResume();
        Refs.resumeSession(this);
    }

    /**
     @name onStop
     @desc We will stop the SDK session state when the app go to background from foreground
     */
    @Override
    protected void onStop() {
        super.onStop();
        Refs.stopSession(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Refs.endSession();

    }
}

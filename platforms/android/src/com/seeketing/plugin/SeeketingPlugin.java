package com.seeketing.plugin;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.seeketing.sdks.sets.Sets;

public class SeeketingPlugin extends CordovaPlugin {
  	@Override
  	public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
		if (action.equals("trackEventNavi")) {
			final String name = args.getString(0);
			final int percent = Integer.parseInt(args.getString(1));
			final String info = args.getString(2);
			cordova.getThreadPool().execute(new Runnable() {
				public void run() {
					trackEventNavi(name, percent, info, callbackContext);
				}
			});
			return true;
		} else if (action.equals("trackEventText")) {
			final String name = args.getString(0);
			final String params = args.getString(1);
			final String info = args.getString(2);
			cordova.getThreadPool().execute(new Runnable() {
				public void run() {
					trackEventText(name, params, info, callbackContext);
				}
			});
			return true;
		} else if (action.equals("trackEventClick")) {
			final String name = args.getString(0);
			cordova.getThreadPool().execute(new Runnable() {
				public void run() {
					trackEventClick(name, callbackContext);
				}
			});
			return true;
		} else {
			Log.d("SeeketingPlugin", "Función errónea");
			return false;
		}
  	}

  	private void trackEventNavi(String name, int percent, String info, CallbackContext callbackContext) {
  		if (name == null || name.length() == 0) {
	  		callbackContext.error("Empty name!");
		} else {
			Sets.trackEventNavi(name, percent, info);
			callbackContext.success(name);
		}
  	}

  	private void trackEventText(String name, String params, String info, CallbackContext callbackContext) {
  		if (name == null || name.length() == 0) {
	  		callbackContext.error("Empty message!");
		} else {
			Sets.trackEventText(name, params, info);
	  		callbackContext.success(name);
		}
  	}

  	private void trackEventClick(String name, CallbackContext callbackContext) {
  		if (name == null || name.length() == 0) {
	  		callbackContext.error("Empty message!");
		} else {
			Sets.trackEventClck(name);
	  		callbackContext.success(name);
		}
  	}
}
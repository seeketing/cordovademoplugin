var exec = require('cordova/exec');

exports.trackEventNavi = function(name, percent, info, success, error) {
    exec(success, error, "SeeketingPlugin", "trackEventNavi", [name, percent, info]);
}; 

exports.trackEventText = function(name, params, info, success, error) {
    exec(success, error, "SeeketingPlugin", "trackEventText", [name, params, info]);
};

exports.trackEventClick = function(name, success, error) {
    exec(success, error, "SeeketingPlugin", "trackEventClick", [name]);
};